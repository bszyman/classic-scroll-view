//
//  AppDelegate.m
//  Classic Scrolling2
//
//  Created by Ben Szymanski on 11/12/17.
//  Copyright © 2017 Ben Szymanski. All rights reserved.
//

#import "AppDelegate.h"
#import "CCIScrollView.h"
#import "CCIScrollbarArrowButton.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;

@property (nonatomic, strong) CCIScrollView *scrollView;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
    CCIScrollView *scrollView = [[CCIScrollView alloc] initWithFrame:NSMakeRect(50.0, 50.0, 315.0, 300.0)];

    [[[self window] contentView] addSubview:scrollView];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
